# Ruff Docker Image

[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://docs.astral.sh/ruff/)
[![Pipeline](https://lab.frogg.it/swepy/containers/ruff/badges/main/pipeline.svg)](https://lab.frogg.it/swepy/containers/ruff/-/pipelines?ref=main)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/ruff-docker-image/-/blob/main/LICENSE)

[![Static Badge](https://img.shields.io/badge/src-Frogg.it-green)](https://lab.frogg.it/swepy/containers/ruff)
[![Static Badge](https://img.shields.io/badge/mirror-GitLab.com-orange)](https://gitlab.com/swepy/containers/ruff)

This Docker image provides a ready-to-use environment with Ruff installed. Ruff is a
highly efficient Python linter and code formatter that significantly speeds up code
checking and formatting tasks. Built with Rust, Ruff offers performance that is orders
of magnitude faster than traditional tools
like [Flake8](https://flake8.pycqa.org/en/latest/)
or [Black](https://black.readthedocs.io/en/stable/index.html).

## Security

### All versions

[CVE-2018-20225⁠](https://scout.docker.com/vulnerabilities/id/CVE-2018-20225): This only
affects use of the --extra-index-url option, which is not used to install ruff in the
image.

## CICD

We rely on GitLab CI components to build and release this image:

[![Release by Changelog](https://img.shields.io/badge/CICD-Release%20By%20Changelog-white?logo=gitlab)](https://gitlab.com/explore/catalog/swepy/cicd-templates/release-by-changelog)
[![Docker Build](https://img.shields.io/badge/CICD-Docker%20Build-white?logo=docker)](https://gitlab.com/explore/catalog/swepy/cicd-templates/docker-build)
[![PyPI to Docker missing version](https://img.shields.io/badge/CICD-PyPI%20to%20Docker%20missing%20version-white?logo=python)](https://gitlab.com/explore/catalog/swepy/cicd-templates/pypi-to-docker-missing-version)
[![Kaniko](https://img.shields.io/badge/backend-Kaniko-orange?logo=google)](https://github.com/GoogleContainerTools/kaniko)
