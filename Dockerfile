# This dockerfile can generate a docker image with ruff since ruff firsts version,
# with a few changes in configuration. Those changes are required since early versions
# of ruff compilation stage rely on "open64", wich is not available in recent alpine
# linux.


ARG APP_ROOT=/app
ARG APP_USER=app
ARG APP_UID=1000
ARG APP_GID=1000

# Default environment with a non priviledged user.
# For ruff < 0.0.20, uncomment following line
#FROM python:3.12.0-alpine3.18 as base
# For ruff < 0.0.20, comment following line
FROM python:3.12.4-alpine3.20 as base

ARG APP_ROOT
ARG APP_USER
ARG APP_UID
ARG APP_GID

RUN mkdir -p $APP_ROOT/.local/bin && \
    adduser $APP_USER -h $APP_ROOT -u $APP_UID -g $APP_GID -DH && \
    chown -R $APP_UID:$APP_GID $APP_ROOT  # force the user to be the owner of the app directory

FROM base as builder

ARG VERSION=""
ARG APP_UID
ARG APP_ROOT

ENV VERSION=${VERSION}
ENV PATH=$APP_ROOT/venv/bin:$PATH

# For ruff < 0.0.20, uncomment following line
#RUN  apk add --no-cache maturin cargo

# Never build with root because we don't need priviledged access to
# build, preventing external source code from running as root.
USER $APP_UID

WORKDIR $APP_ROOT

RUN python3 -m venv --upgrade-deps $APP_ROOT/venv

# disable pip cache to reduce the image size
RUN pip install --prefer-binary --no-cache-dir ruff==${VERSION}


FROM base

ARG APP_ROOT
ARG APP_USER
ARG APP_UID
ARG APP_GID
ARG VERSION=""

# For ruff < 0.0.20, uncomment following line
#RUN apk add libgcc

USER $APP_UID
WORKDIR $APP_ROOT

ENV PATH=$APP_ROOT/venv/bin:$PATH
ENV VERSION=${VERSION}
COPY --from=builder --chown=$APP_UID:$APP_GID $APP_ROOT/venv ./venv

LABEL maintainers="Dorian Turba <contact.docker.cblm4@simplelogin.com>, osoc@devkontrol.slmail.me"
LABEL description="Lightweight image to run Ruff, the extremely fast Python linter and code formatter, written in Rust."
LABEL version="${VERSION}"
LABEL license="MIT"
LABEL org.opencontainers.image.source="https://lab.frogg.it/swepy/containers/ruff"
LABEL org.opencontainers.image.source.gitlab_com="https://gitlab.com/swepy/containers/ruff"
LABEL org.opencontainers.image.issues="https://lab.frogg.it/swepy/containers/ruff/-/issues"
LABEL org.opencontainers.image.created="$(date -u +'%Y-%m-%dT%H:%M:%SZ')"
LABEL org.opencontainers.image.title="Ruff"
LABEL org.opencontainers.image.url="https://astral.sh/ruff"
LABEL org.opencontainers.image.revision="0.1.0"
