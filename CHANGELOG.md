# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.6] - 2024-07-16

* Fix of auto release

## [2.0.5] - 2024-07-16

* Update of CICD

## [2.0.4] - 2024-07-13

* Update of CICD
* Update of README to match dockerhub

## [2.0.3] - 2024-07-03

### Fixed

* New versions will now be release during the `deploy` stage

## [2.0.2] - 2024-07-03

### Fixed

* Job name preventing the pipeline to run

## [2.0.1] - 2024-06-26

### Fixed

* Prevent old images to be build when docker API fails to return all versions

## [2.0.0] - 2024-06-25

### Changed

* Dockerfile no longer install cargo and maturin for ruff compilation, making the 
  image building process quicker, at the expense of not being able to build ruff
  for version lower than 0.0.20

## [1.0.0] - 2024-06-24

* Initial version
